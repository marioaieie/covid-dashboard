# -*- coding: utf-8 -*-

# Run this app with `python app.py` and
# visit http://127.0.0.1:8050/ in your web browser.

import dash
import dash_core_components as dcc
import dash_html_components as html
import plotly.express as px
import pandas as pd
import numpy as np
import logging
# from uk_covid19 import Cov19API

external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']

app = dash.Dash(__name__, external_stylesheets=external_stylesheets)

server = app.server

logging.basicConfig(format='[%(asctime)s] [%(process)d] [%(levelname)s] %(message)s', level=logging.INFO)


# assume you have a "long-form" data frame
# see https://plotly.com/python/px-arguments/ for more options

def load_global_data():
    # Get the population of each country
    countries = pd.read_csv('population.csv')
    countries = countries[countries['Year'] == 2018]
    # Some countries have names that differ from the one used in the covid dataset
    countries.replace({'United States': 'US', 'Russian Federation': 'Russia', 'Czech Republic': 'Czechia'},
                      inplace=True)

    # Read in the data
    logging.info('Loading global data...')
    filename = 'https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_confirmed_global.csv'
    df = pd.read_csv(filename, )
    #                  parse_dates=['data'])
    logging.info('Loaded global data.')

    df = df.melt(id_vars=['Province/State', 'Country/Region', 'Lat', 'Long'],
                 var_name='date', value_name='cases')

    df = df.groupby(['Country/Region', 'date'])['cases'].sum().reset_index()
    df.rename(columns={'Country/Region': 'country'}, inplace=True)
    df['date'] = pd.to_datetime(df['date'], dayfirst=False, format='%m/%d/%y')

    # Add the country population
    df = df.merge(countries[['Country Name', 'Value']], left_on='country', right_on='Country Name', how='left', )

    top_cases_eu_ish_countries = ['Spain',
                                  'Italy',
                                  'France',
                                  'United Kingdom',
                                  'Germany',
                                  'Belgium',
                                  'Netherlands',
                                  'Switzerland',
                                  'Portugal',
                                  'Ireland',
                                  'Sweden',
                                  'Austria',
                                  ]
    df_euish = df[df['country'].isin(top_cases_eu_ish_countries)].groupby(['date'])[
        ['cases', 'Value']].sum().reset_index()
    df_euish['country'] = 'EU-ish'
    df = df.append(df_euish, ).reset_index(drop=True)

    column_map = {'country': 'geo_region',
                  'Value': 'inhabitants'}
    df.rename(columns=column_map, inplace=True)

    return df


def compute_derivatives(df):
    logging.info('Processing data...')

    df.sort_values(['date'], ascending=True, inplace=True)

    df['first_derivative'] = df.groupby('geo_region')['cases'].diff()
    df['growth_factor'] = df['first_derivative'] / df.groupby('geo_region')['first_derivative'].shift(1)
    df['cases_smooth'] = df.groupby(['geo_region'])['cases'].rolling(7).sum().reset_index(level=0, drop=True) / 7
    df['first_derivative_smooth'] = df.groupby(['geo_region'])['first_derivative'].rolling(7).sum().reset_index(level=0,
                                                                                                             drop=True)
    df['growth_factor_smooth'] = df.groupby(['geo_region'])['growth_factor'].rolling(7).mean().reset_index(level=0,
                                                                                                        drop=True)
    df['cases_per_inhabitant'] = df['cases'] / df['inhabitants'] * 100000
    df['cases_smooth_per_inhabitant'] = df['cases_smooth'] / df['inhabitants'] * 100000
    df['first_derivative_smooth_per_inhabitant'] = df['first_derivative_smooth'] / df['inhabitants'] * 100000
    df['growth_factor_smooth_per_inhabitant'] = df['growth_factor_smooth'] / df['inhabitants'] * 100000

    logging.info('Processed data.')

    return df


def load_italian_province_data():
    filename = 'https://raw.githubusercontent.com/pcm-dpc/COVID-19/master/dati-province/dpc-covid19-ita-province.csv'
    df = pd.read_csv(filename, parse_dates=['data'])

    # df.sort_values('data', inplace=True)

    df['inhabitants'] = np.nan
    column_map = {'data': 'date',
                  'totale_casi': 'cases',
                  'denominazione_provincia': 'geo_region'}
    df.rename(columns=column_map, inplace=True)
    df = df[~df['geo_region'].isin(['Fuori Regione / Provincia Autonoma', 'In fase di definizione/aggiornamento'])]

    return df


def load_italian_data(filename):
    logging.info('Loading italian data...')
    df = pd.read_csv(filename, parse_dates=['data'])
    logging.info('Loaded italian data.')

    df_ita = df.groupby(['data', 'stato']).sum().reset_index()
    df_ita['denominazione_regione'] = 'Italia'
    df = df.append(df_ita, ignore_index=True, ).reset_index(drop=True)

    # df.sort_values('data', inplace=True)

    df['inhabitants'] = np.nan
    column_map = {'data': 'date',
                  'totale_casi': 'cases',
                  'denominazione_regione': 'geo_region'}
    df.rename(columns=column_map, inplace=True)

    return df


# Load data for countries
df_global = load_global_data()
df_global = compute_derivatives(df_global)
country_options = [{'label': country, 'value': country}
                   for country in sorted(df_global['geo_region'].unique())]


# Load data for italian regions
filename = 'https://raw.githubusercontent.com/pcm-dpc/COVID-19/master/dati-regioni/dpc-covid19-ita-regioni.csv'
# filename = 'https://raw.githubusercontent.com/pcm-dpc/COVID-19/master/dati-regioni/dpc-covid19-ita-regioni-latest.csv'
df_italy = load_italian_data(filename)
df_italy = compute_derivatives(df_italy)
region_options = [{'label': region, 'value': region}
                  for region in df_italy['geo_region'].sort_values().unique()]

logging.info('Ready to plot.')


def update_graph(df, region_list):
    if len(region_list) == 0:
        region_list = df['geo_region'].sort_values().unique()
    x = 'cases_smooth_per_inhabitant' if df['inhabitants'].notnull().any() else 'cases_smooth'
    y = 'first_derivative_smooth_per_inhabitant' if df['inhabitants'].notnull().any() else 'first_derivative_smooth'
    fig = px.line(df[df['geo_region'].isin(region_list)],
                  x, y,
                  line_dash='geo_region',
                  color='geo_region', hover_name='date', log_y=True, log_x=True,
                  labels={'first_derivative_smooth_per_inhabitant': 'Weekly new cases (x100000 habitants)',
                          'cases_smooth_per_inhabitant': 'Total cases (smoothed, x100000 habitants)',
                          'cases_per_inhabitant': 'Total cases (x100000 habitants)',
                          'first_derivative_smooth': 'Weekly new cases',
                          'cases_smooth': 'Total cases (smoothed)'
                          },
                  )
    logging.info('Updated plot.')

    return fig


# @app.callback(
#     dash.dependencies.Output('italian-plot', 'figure'),
#     [dash.dependencies.Input('crossfilter-region', 'value'), ])
# def update_italian_graph(region_list):
#     logging.info('Updating italian plot...')
#     return update_graph(df_italy, region_list)


@app.callback(
    dash.dependencies.Output('global-plot', 'figure'),
    [dash.dependencies.Input('crossfilter-country', 'value'), ])
def update_global_graph(country_list):
    logging.info('Updating global plot...')
    return update_graph(df_global, country_list)


app.layout = html.Div(children=[
    html.H1(children='Covid-19 dashobard'),

    dcc.Markdown('''
        This is my attempt at reproducing the chart from [https://aatishb.com/covidtrends/](https://aatishb.com/covidtrends/).
        Minute physics has an amazing [video](https://www.youtube.com/watch?v=54XLXg4fYsc) explaining why this view on
        the problem makes sense.
    '''),

    html.H2('Global chart'),
    html.Label('Select a country'),
    dcc.Dropdown(
        id='crossfilter-country',
        options=country_options,
        value=['Italy', 'Germany', 'Spain', 'United Kingdom'],
        multi=True
    ),
    dcc.Graph(
        id='global-plot',
    ),

    html.H2('Italian regions chart'),
    html.Label('Select a region'),
    dcc.Dropdown(
        id='crossfilter-region',
        options=region_options,
        value=[],
        multi=True
    ),
    dcc.Graph(
        id='italian-plot',
    ),

    # html.H2('Italian provinces chart'),
    # html.Label('Select a province'),
    # dcc.Dropdown(
    #     id='crossfilter-province',
    #     options=province_options,
    #     value=[],
    #     multi=True
    # ),
    # dcc.Graph(
    #     id='italian-province-plot',
    # ),

    # html.H2('UK regions chart'),
    # html.Label('Select a region'),
    # dcc.Dropdown(
    #     id='crossfilter-uk-region',
    #     options=uk_region_options,
    #     value=[],
    #     multi=True
    # ),
    # dcc.Graph(
    #     id='uk-region-plot',
    # ),
])

if __name__ == '__main__':
    logging.info('Starting up server...')
    app.run_server(debug=False)
    logging.info('Started up server.')

